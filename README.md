# AutoScaling and LoadBalancer in AWS and DNS/Route 53

**Autoscaling** allows you to always have a number of health servers and launches new ones at set parameters.

**LoadBalancing** allows you to distribute traffic to number of servers.

**DNS/route53** Domain name service, gives your ip a name. Route 53 is a service that allows you to register domains and manage the DNS records. 

## Loadbalancer

**LoadBalancing** allows you to distribute traffic to number of servers. To set this up you need a `target group` of server to balance traffic between. 

Their are a few types of load balancers that operate at different protocols / levels of the OSI model. We used layer 7 - https and http - that is the fastes and cheapest.

### Target groups

These are lists of machines or IP (once again linked to OSI model) that your Loadbalancer will balance between.

#### **Setup**

#### Step 1 - type of loadbalances

There are a few types of target groups that can be created - this follows the OSI model. We want the one that target instances.


#### step 2 - health check option

You also define protocols of where to check if operational (http and port 80 for example).

Target groups also have health check options where you define how often to ping a server, where, how long before time out, how many times to get a healthy status and how many times to ping before an unhealthy status.

#### Step 3 - Registering targets

You can then register the targets to be in target group. This is usually the machines between which you'll load balance.

**Note** to have an autoscale group, it is useful to have a Loadbalancer, **however**, the **´target group´ cannon have any targets specified** as the autoscale will create new machines that will be automatically assigned to the target group. It still needs a target group. 

## Autoscaling

**Autoscaling** allows you to always have a number of health servers and launches new ones at set parameters.

To autoscale we will need: 

- launch template 
- Loadbalancer
- Target group 


#### Step by step for AutoScale 

#### Step 1) Launch template (create one or use existing)

This is a template of how new machines inside the autoscale will launche. You'll need:

- AMI
- Security groups
- Keypair
- What instance (t2.micro)
- What availability zones & subnet
- storage
- user data (init scripts)

**You do not need a new network interface**

**user data (init scripts)** in Advanced setting you can send in some "user data" which we we'll use to start services and start software.

#### Step 2 Launch location

Specify the subnet, vpc and availability zones.

#### Step 3 Loadbalance

Attache existing one or create one. It should:

- be internet facing
- have a target group with not targets set
- have a health check that is appropriate


#### Step 4 Configure group size and scaling policies

This is where you define:

- Mininum number of machine 
- Maximum number of servers (Scale out)
- Desired State (Where it tries to scale in and out to)

Also difine how to decine when to scale out (create more server) or scale in (destroy servers):

- CPU
- Network usage

## DNS and Route 53

DNS - Domain name Service
You do not know every single phone number on your phone. However, you know people's names and you can search for that. It is easier to rememeber.

Domain name is like a phonebook that is distributed globally.

Point Names to IPs.

Domanin name Service providers - allow you to reserve name, and then point them to what ever servers you want. 

Example of Domain Name Service Providers:

- domain.com
- godaddy.com
- gandi.net
- name123.com
- namecheap.com 


These keep allow you to write to a ledgures of who own what name! You then register a name with ICANN.

ICANN is a non for profit huge organisation you'll never hearabout that makes legislation around domain names.

When you stop paying some domains they go through a process of administration, you can recue them and pay a fine, or eventually they revert back to a pool of names available kept by ICANN.

Should there be a domain name becoming available there is an entire industry built around "snaping" these name for you. 

And also buying and selling domain names.


### Whois 

Is a service, you should be sure to use a trusted one, which check open records of the ownership of a domain.

-https://lookup.icann.org/


### DNS Records to set 

These are different proctols that you can set as an owner. 

Including:

- A records (ip)
- AAAA records ()
- CNAME (domain_name)
- MX Records (mail exchange)

Once you set them, they take time to propegate. 

You can check the records of a domain name using a domain name record checker.

https://www.whatsmydns.net/

Let say we buy catdog.ninja

We can then buy some gmail email boxes and other software. 
Then we want your emails to be filipe@catdog.co.uk
We need to point our DNS mx records to where ever the google MX servers are.

### Route 53 

This is the DNS provider of AWS. 

We can make buy domains and make sub domains and manage the Records.

#### Updside of Route53

- Instant replication
- Allows to route user from different locations to close by servers. 
- Allows to have failovers and other resiliance mechanism setup



## Terraform

In terraform we need to declare the things with did with the AWS console. 

We will need these resources:

- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group_attachment
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb

